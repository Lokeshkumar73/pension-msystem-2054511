export class PensionDetail {
    constructor(
        public name: String,
        public dateOfBirth: Date,
        public pan: String,
        public pensionType: String,
        public serviceCharge: number,
        public pensionAmount: number
    ) { }
}
