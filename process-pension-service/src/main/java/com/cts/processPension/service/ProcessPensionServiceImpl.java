package com.cts.processPension.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cts.processPension.exception.NotFoundException;
//import com.cts.processPension.feign.PensionDisbursementClient;
import com.cts.processPension.feign.PensionerDetailsClient;
import com.cts.processPension.model.PensionAmountDetail;
//import com.cts.processPension.model.PensionAmountDetail;
import com.cts.processPension.model.PensionDetail;
import com.cts.processPension.model.PensionerDetail;
import com.cts.processPension.model.PensionerInput;
import com.cts.processPension.repository.PensionAmountRepository;
//import com.cts.processPension.model.ProcessPensionInput;
//import com.cts.processPension.model.ProcessPensionResponse;
//import com.cts.processPension.repository.PensionDetailsRepository;
import com.cts.processPension.repository.PensionerDetailsRepository;

import lombok.extern.slf4j.Slf4j;


 
@Service
@Slf4j
public class ProcessPensionServiceImpl implements IProcessPensionService {

	@Autowired
	private PensionerDetailsClient pensionerDetailClient;

	@Autowired
	private PensionAmountRepository pensionAmountRepository;
	
	@Autowired
	private PensionerDetailsRepository pensionerDetailsRepository;

	@Override
	public PensionDetail getPensionDetails(PensionerInput pensionerInput) {

		// get the pensioner details from the detail micro-service
		PensionerDetail pensionerDetail = pensionerDetailClient
				.getPensionerDetailByAadhaar(pensionerInput.getAadhaarNumber());

		log.info("Details found by details microservice");

		// check if the entered details are correct
		if (checkdetails(pensionerInput, pensionerDetail)) {
			// save the input pensioner details into the database
			pensionerDetailsRepository.save(pensionerInput);
			// calculate the amount and return the pension detail object
			return calculatePensionAmount(pensionerDetail);
		} else {
			throw new NotFoundException("Details entered are incorrect");
		}
	}

	/**
	 * Calculate the pension amount and return the pensioner details according to
	 * the type of pension "self" or "family"
	 * @param Verified Pensioner Details
	 * @return Pension Details with Pension amount
	 */
	@Override
	public PensionDetail calculatePensionAmount(PensionerDetail pensionDetail) {
		double pensionAmount = 0;
		double serviceCharge=0;
		if(pensionDetail.getBank().getBankType().equalsIgnoreCase("public"))
			serviceCharge=500.0;
		else
			serviceCharge=550.0;
		if (pensionDetail.getPensionType().equalsIgnoreCase("self"))
			pensionAmount = (pensionDetail.getSalary() * 0.8 + pensionDetail.getAllowance());
		else if (pensionDetail.getPensionType().equalsIgnoreCase("family"))
			pensionAmount = (pensionDetail.getSalary() * 0.5 + pensionDetail.getAllowance());
		PensionAmountDetail pensionAmountDetail = new PensionAmountDetail(pensionDetail.getName(),pensionDetail.getPan(),pensionAmount,serviceCharge);
		pensionAmountRepository.save(pensionAmountDetail);
		return new PensionDetail(pensionDetail.getName(), pensionDetail.getDateOfBirth(), pensionDetail.getPan(),
				pensionDetail.getPensionType(),serviceCharge, pensionAmount);
	}


	@Override
	public boolean checkdetails(PensionerInput pensionerInput, PensionerDetail pensionerDetail) {
		return (pensionerInput.getName().equalsIgnoreCase(pensionerDetail.getName())
				&& (pensionerInput.getDateOfBirth().compareTo(pensionerDetail.getDateOfBirth()) == 0)
				&& pensionerInput.getPan().equalsIgnoreCase(pensionerDetail.getPan())
				&& pensionerInput.getPensionType().equalsIgnoreCase(pensionerDetail.getPensionType()));
	}

}