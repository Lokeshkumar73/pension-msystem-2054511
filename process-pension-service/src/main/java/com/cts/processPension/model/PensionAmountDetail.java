package com.cts.processPension.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Data to be stored in the database
 */
@NoArgsConstructor
@Setter
@Getter
@AllArgsConstructor
@Entity
public class PensionAmountDetail {

	@Id
	private String name;
	
	@Column
	private String pan;

	@Column
	private Double pensionAmount;

	@Column
	private Double bankServiceCharge;

}
